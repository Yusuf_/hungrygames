import model.CareerContestant;
import model.Contestant;
import model.DistrictContestant;
import model.Gender;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MyTests {
    private ArenaController arenaController;

    @Before
    public void setup(){
        arenaController = new ArenaController();
    }

    @Test
    public void createDistrictContestant(){
        Contestant contestant = ContestantFactory.getContestant("district");
        System.out.println(contestant);
        Assert.assertTrue(contestant instanceof DistrictContestant);
    }

    @Test
    public void createCareerContestant(){
        Contestant contestant = ContestantFactory.getContestant("career");
        System.out.println(contestant);
        Assert.assertTrue(contestant instanceof CareerContestant);
    }

    @Test
    public void careerContestantGetBattleItemByCreation(){
        List<Contestant> contestants = arenaController.createContestants(1, "career", Gender.MALE);
        Contestant contestant = contestants.get(0);
        int initialHealth = 100;
        int initialAttack = 150;
        Assert.assertTrue(contestant.getHealth() > initialHealth || contestant.getAttack() > initialAttack);
    }

    @Test
    public void createTwelveMaleCContestants(){
        List<Contestant> contestants = arenaController.createContestants(12, "career", Gender.MALE);
        int expectedSize = 12;
        int actualSize = contestants.size();
        Assert.assertEquals(expectedSize, actualSize);
        Contestant contestant = contestants.get(0);
        Assert.assertEquals(contestant.getGender(), Gender.MALE);
    }
}
