
import model.*;

import java.util.*;

public class ArenaController {
    private List<Contestant> contestants;
    private final double BATTLE_CHANCE = 0.2;
    private final double LOOT_CHANCE = 0.2;
    private final int BOOST_LIMIT = 50;
    private Random random;

    public ArenaController() {
        contestants = new ArrayList<Contestant>();
        random = new Random();
    }

    /**
     * Returns a list of contestants that will fight in the hungry games. You need to give the amount, type of
     * contestants and gender.
     * @param amount the amount of contestants that will be created.
     * @param type the type of contestant. This can be "career" of "district"
     * @param gender the gender of the contestant. This is an enum that cen be "female" or "male". More genders will
     * be added in the future.
     * @return list of contestants.
     */
    public List<Contestant> createContestants(int amount, String type, Gender gender){
        List<Contestant> contestants = new ArrayList<Contestant>();
        for (int i = 0; i < amount; i++) {
            Contestant contestant = ContestantFactory.getContestant(type);
            contestant.setId(i);
            contestant.setGender(gender);

            if(contestant instanceof CareerContestant)
                findRandomBattleItem(contestant);

            int health = contestant.getHealth() + ( random.nextInt(BOOST_LIMIT));
            contestant.setHealth(health);

            int attack = contestant.getAttack() + (random.nextInt(BOOST_LIMIT));
            contestant.setAttack(attack);

            double chance = random.nextDouble();
            contestant.setChance(chance);

            contestants.add(contestant);
        }
        return contestants;
    }

    /**
     * This will setup the infamous Hungry games. The contestants are created and added to the list of participating
     * contestants. They will fight to the death till one contestant remains.
     */
    private void setup(){
        List<Contestant> careerMaleContestants = createContestants(6, "career", Gender.MALE);
        List<Contestant> districtMaleContestants = createContestants(6, "district", Gender.MALE);
        List<Contestant> districtFemaleContestants = createContestants(12, "district", Gender.FEMALE);
        contestants.addAll(careerMaleContestants);
        while(contestants.size() > 1){
            System.out.println("Current remaining contestants " + contestants.size());
            if(random.nextDouble() > BATTLE_CHANCE){
                Collections.shuffle(contestants);
                battle(contestants.remove(0), contestants.remove(0));
            } else {
                System.out.println("No battle this round");
            }
        }
        System.out.println("**************************");
        Contestant winner = contestants.get(0);
        System.out.println("Remaining " + winner);
        System.out.println("[President Snow] >> BRUTALITY >> [" + winner.getId() + "]");
        System.out.println("**************************");


    }

    /**
     * Two contestants will fight to death. The contestant with the highest chance gets one extra attack.
     * @param contestantOne the attacking contestant
     * @param contestantTwo the defending contestant
     */
    private void battle(Contestant contestantOne, Contestant contestantTwo){
        if(contestantOne.getChance() > contestantTwo.getChance()){
            System.out.println("[" + contestantOne.getId() + "] gets free first strike!");
            fight(contestantOne ,contestantTwo);
        } else {
            System.out.println("[" + contestantOne.getId() + "] gets free first strike!");
            fight(contestantTwo, contestantOne);
        }

        do {
            fight(contestantOne, contestantTwo);
            fight(contestantTwo, contestantOne);
        } while (contestantOne.getHealth() > 0 && contestantTwo.getHealth() > 0);

        if(contestantOne.getHealth() > 0){
            System.out.println("[" + contestantOne.getId() + "] wins!");
            endMatch(contestantOne);
        } else if(contestantTwo.getHealth() > 0){
            System.out.println("[" + contestantTwo.getId() + "] wins!");
            endMatch(contestantTwo);
        }
        System.out.println("=============================");
    }

    /**
     * The winning contestant's health is reset to full. If the contestant is lucky he will get a battle item that will
     * boost his health or attack.
     * @param contestant winning contestant
     */
    private void endMatch(Contestant contestant){
        contestant.setHealth(100);
        if(random.nextDouble() > LOOT_CHANCE){
            findRandomBattleItem(contestant);
            contestants.add(contestant);
        }
    }

    /**
     * Create random battle item and assign to a contestant. The battle item can be a potion or weapon.
     * @param contestant the
     */
    private void findRandomBattleItem(Contestant contestant){
        BattleItem battleItem = random.nextBoolean() ? new Potion() : new Weapon();
        battleItem.use(contestant);
    }

    /**
     * Fight between two contestants till their health is above 0.
     * @param contestantOne attacking contestant that will deal damage.
     * @param contestantTwo defending contestant that will receive damage.
     */
    private void fight(Contestant contestantOne, Contestant contestantTwo){
        if(contestantOne.getHealth() > 0){
            int damage = contestantOne.getAttack() - contestantTwo.getDefense();
            int health = contestantTwo.getHealth() - damage;

            System.out.println("[" + contestantOne.getId() + "] >> " + damage + " >> [" + contestantTwo.getId() + "]");
            System.out.println("[" + contestantTwo.getId() + "] Health: " + health);
            contestantTwo.setHealth(health);
        }
    }


    public static void main(String[] args) {
        ArenaController arena = new ArenaController();
        arena.setup();
    }

}
