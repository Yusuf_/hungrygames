package model;

public interface BattleItem {
    void use(Contestant contestant);
}
