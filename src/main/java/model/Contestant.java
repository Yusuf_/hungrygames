package model;

import lombok.Data;

@Data
public abstract class Contestant {
    private int id;
    private int attack;
    private int defense;
    private double chance;
    private int health;
    private Gender gender;

    public Contestant() {
        this.attack = 100;
        this.defense = 100;
        this.health = 100;
    }
}
