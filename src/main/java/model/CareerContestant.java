package model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class CareerContestant extends Contestant {

    public CareerContestant() {
        this.setAttack(150);
    }
}
