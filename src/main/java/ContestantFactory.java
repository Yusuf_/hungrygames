import model.CareerContestant;
import model.Contestant;
import model.DistrictContestant;

public class ContestantFactory {

    public static Contestant getContestant(String type){
        if(type.equals("career")){
            return new CareerContestant();
        }
        else if(type.equals("district")){
            return new DistrictContestant();
        }
        return null;
    }
}
